#!/bin/sh

if [ $# -ne 3 ]; then
  echo "Usage: $0 file1 file2 startline"
  exit 1
fi

file1="$1"
file2="$2"
startline="$3"

if [ ! -f "$file1" ]; then
  echo "File '$file1' not found."
  exit 1
fi

if [ ! -f "$file2" ]; then
  echo "File '$file2' not found."
  exit 1
fi

if ! [ "$startline" -eq "$startline" ] 2>/dev/null; then
  echo "startline must be a valid integer."
  exit 1
fi

# Check if the last line of file1 is empty, and if not, add one
if [ "$(tail -n 1 "$file1")" != "" ]; then
  echo "" >> "$file1"
fi

# Create a temporary file for the modified file2
tmpfile=$(mktemp)

# Insert the text contents of file1 into file2 at startline
sed "${startline}r $file1" "$file2" > "$tmpfile"

# Move the temporary file back to file2
mv "$tmpfile" "$file2"

# Remove the original line from file2 that was at startline before the insertion
sed -i "${startline}d" "$file2"

echo "Text from '$file1' inserted into '$file2' at line $startline, and the original line removed."
