# manteiga.sh

## É na manteiga... manteiga.

Helper script to setup my btrfs sub-volumes layout.

## Docs here

[First doc entry](doc/zettlr.md){:target="_blank"}

## Changelog

### v0.0.1

Just a bump in the incoming road. Hope this script doesn't get forgotten.