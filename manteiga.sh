#!/bin/sh

#
# Command line script for setup btrfs things during fresh new install
#
# For @cerberus machine only. Need to test support using it on other machines
# 

# Download this like below
# wget gitlab.com/aviler/manteiga/-/raw/develop/manteiga.sh -O /tmp/manteiga.sh

# You just did a `df` to make sure our `/dev/mapper/vg-lv` partition
# or is it a volume? :side_eyes: 

# Undo some defaults
umount /target/boot/efi/ &&
umount /target/boot/ &&
umount /target/ &&

# Mount the encrypted one
mount /dev/mapper/vg-lv /mnt &&

# Move the root
mv /mnt/@rootfs/ /mnt/@ &&

# Add new volumes. Mainly to make sure that these directories/volumes
# Do not get snapshot'ed
btrfs su cr /mnt/@snapshots &&
btrfs su cr /mnt/@home &&
btrfs su cr /mnt/@opt &&
btrfs su cr /mnt/@srv &&
btrfs su cr /mnt/@swap &&
btrfs su cr /mnt/@tmp &&
btrfs su cr /mnt/@usrlocal &&
btrfs su cr /mnt/@log &&
btrfs su cr /mnt/@cache &&
btrfs su cr /mnt/@crash &&
btrfs su cr /mnt/@vartmp &&
btrfs su cr /mnt/@spool &&
btrfs su cr /mnt/@images &&
btrfs su cr /mnt/@containers &&
btrfs su cr /mnt/@AccountsService &&
btrfs su cr /mnt/@gdm3 &&
btrfs su cr /mnt/@portables &&
btrfs su cr /mnt/@machines &&
btrfs su cr /mnt/@homeaviler &&
btrfs su cr /mnt/@avilerdrive &&
btrfs su cr /mnt/@avilerdownloads &&
btrfs su cr /mnt/@avilercache &&
btrfs su cr /mnt/@avilerlocal &&
btrfs su cr /mnt/@avilercontainers &&
btrfs su cr /mnt/@avilerboxes &&
btrfs su cr /mnt/@avilersnapshots &&
btrfs su cr /mnt/@avilervar &&
btrfs su cr /mnt/@avilerxdg &&
btrfs su cr /mnt/@avilerapps &&
btrfs su cr /mnt/@homeguest &&
btrfs su cr /mnt/@guestdrive &&
btrfs su cr /mnt/@guestdownloads &&
btrfs su cr /mnt/@guestcache &&
btrfs su cr /mnt/@guestlocal &&
btrfs su cr /mnt/@guestcontainers &&
btrfs su cr /mnt/@guestboxes &&
btrfs su cr /mnt/@guestsnapshots &&
btrfs su cr /mnt/@guestvar &&
btrfs su cr /mnt/@guestxdg &&
btrfs su cr /mnt/@guestapps &&

# Wow we need to mount this `/target` that we talk some much about it below
mount -o noatime,compress=zstd:1,subvol=@ /dev/mapper/vg-lv /target &&


mkdir -p /target/.snapshots &&
mount -o noatime,compress=zstd:1,subvol=@snapshots /dev/mapper/vg-lv /target/.snapshots &&
mkdir -p /target/home &&
mount -o noatime,compress=zstd:1,subvol=@home /dev/mapper/vg-lv /target/home &&
mkdir -p /target/opt &&
mount -o noatime,compress=zstd:1,subvol=@opt /dev/mapper/vg-lv /target/opt &&
mkdir -p /target/srv &&
mount -o noatime,compress=zstd:1,subvol=@srv /dev/mapper/vg-lv /target/srv &&
mkdir -p /target/swap &&
mount -o noatime,compress=zstd:1,subvol=@swap /dev/mapper/vg-lv /target/swap &&
mkdir -p /target/tmp &&
mount -o noatime,compress=zstd:1,subvol=@tmp /dev/mapper/vg-lv /target/tmp &&
mkdir -p /target/usr/local &&
mount -o noatime,compress=zstd:1,subvol=@usrlocal /dev/mapper/vg-lv /target/usr/local &&
mkdir -p /target/var/log &&
mount -o noatime,compress=zstd:1,subvol=@log /dev/mapper/vg-lv /target/var/log &&
mkdir -p /target/var/cache &&
mount -o noatime,compress=zstd:1,subvol=@cache /dev/mapper/vg-lv /target/var/cache &&
mkdir -p /target/var/crash &&
mount -o noatime,compress=zstd:1,subvol=@crash /dev/mapper/vg-lv /target/var/crash &&
mkdir -p /target/var/tmp &&
mount -o noatime,compress=zstd:1,subvol=@vartmp /dev/mapper/vg-lv /target/var/tmp &&
mkdir -p /target/var/spool &&
mount -o noatime,compress=zstd:1,subvol=@spool /dev/mapper/vg-lv /target/var/spool &&
mkdir -p /target/var/lib/libvirt/images &&
mount -o noatime,compress=zstd:1,subvol=@images /dev/mapper/vg-lv /target/var/lib/libvirt/images &&
mkdir -p /target/var/lib/containers &&
mount -o noatime,compress=zstd:1,subvol=@containers /dev/mapper/vg-lv /target/var/lib/containers &&
mkdir -p /target/var/lib/AccountsService &&
mount -o noatime,compress=zstd:1,subvol=@AccountsService /dev/mapper/vg-lv /target/var/lib/AccountsService &&
mkdir -p /target/var/lib/gdm3 &&
mount -o noatime,compress=zstd:1,subvol=@gdm3 /dev/mapper/vg-lv /target/var/lib/gdm3 &&
mkdir -p /target/var/lib/portables &&
mount -o noatime,compress=zstd:1,subvol=@portables /dev/mapper/vg-lv /target/var/lib/portables &&
mkdir -p /target/var/lib/machines &&
mount -o noatime,compress=zstd:1,subvol=@machines /dev/mapper/vg-lv /target/var/lib/machines &&
mkdir -p /target/home/aviler &&
mount -o noatime,compress=zstd:1,subvol=@homeaviler /dev/mapper/vg-lv /target/home/aviler &&
mkdir -p /target/home/aviler/Drive &&
mount -o noatime,compress=zstd:1,subvol=@avilerdrive /dev/mapper/vg-lv /target/home/aviler/Drive &&
mkdir -p /target/home/aviler/Downloads &&
mount -o noatime,compress=zstd:1,subvol=@avilerdownloads /dev/mapper/vg-lv /target/home/aviler/Downloads &&
mkdir -p /target/home/aviler/.cache &&
mount -o noatime,compress=zstd:1,subvol=@avilercache /dev/mapper/vg-lv /target/home/aviler/.cache &&
mkdir -p /target/home/aviler/.local &&
mount -o noatime,compress=zstd:1,subvol=@avilerlocal /dev/mapper/vg-lv /target/home/aviler/.local &&
mkdir -p /target/home/aviler/.local/share/containers/storage/ &&
mount -o noatime,compress=zstd:1,subvol=@avilercontainers /dev/mapper/vg-lv /target/home/aviler/.local/share/containers/storage &&
mkdir -p /target/home/aviler/.local/share/gnome-boxes/images/ &&
mount -o noatime,compress=zstd:1,subvol=@avilerboxes /dev/mapper/vg-lv /target/home/aviler/.local/share/gnome-boxes/images &&
mkdir -p /target/home/aviler/.snapshots &&
mount -o noatime,compress=zstd:1,subvol=@avilersnapshots /dev/mapper/vg-lv /target/home/aviler/.snapshots &&
mkdir -p /target/home/aviler/.var &&
mount -o noatime,compress=zstd:1,subvol=@avilervar /dev/mapper/vg-lv /target/home/aviler/.var &&
mkdir -p /target/home/aviler/.xdg &&
mount -o noatime,compress=zstd:1,subvol=@avilerxdg /dev/mapper/vg-lv /target/home/aviler/.xdg &&
mkdir -p /target/home/aviler/Applications &&
mount -o noatime,compress=zstd:1,subvol=@avilerapps /dev/mapper/vg-lv /target/home/aviler/Applications &&
mkdir -p /target/home/guest &&
mount -o noatime,compress=zstd:1,subvol=@homeguest /dev/mapper/vg-lv /target/home/guest &&
mkdir -p /target/home/guest/Drive &&
mount -o noatime,compress=zstd:1,subvol=@guestdrive /dev/mapper/vg-lv /target/home/guest/Drive &&
mkdir -p /target/home/guest/Downloads &&
mount -o noatime,compress=zstd:1,subvol=@guestdownloads /dev/mapper/vg-lv /target/home/guest/Downloads &&
mkdir -p /target/home/guest/.cache &&
mount -o noatime,compress=zstd:1,subvol=@guestcache /dev/mapper/vg-lv /target/home/guest/.cache &&
mkdir -p /target/home/guest/.local &&
mount -o noatime,compress=zstd:1,subvol=@guestlocal /dev/mapper/vg-lv /target/home/guest/.local &&
mkdir -p /target/home/guest/.local/share/containers/storage/ &&
mount -o noatime,compress=zstd:1,subvol=@guestcontainers /dev/mapper/vg-lv /target/home/guest/.local/share/containers/storage &&
mkdir -p /target/home/guest/.local/share/gnome-boxes/images/ &&
mount -o noatime,compress=zstd:1,subvol=@guestboxes /dev/mapper/vg-lv /target/home/guest/.local/share/gnome-boxes/images &&
mkdir -p /target/home/guest/.snapshots &&
mount -o noatime,compress=zstd:1,subvol=@guestsnapshots /dev/mapper/vg-lv /target/home/guest/.snapshots &&
mkdir -p /target/home/guest/.var &&
mount -o noatime,compress=zstd:1,subvol=@guestvar /dev/mapper/vg-lv /target/home/guest/.var &&
mkdir -p /target/home/guest/.xdg &&
mount -o noatime,compress=zstd:1,subvol=@guestxdg /dev/mapper/vg-lv /target/home/guest/.xdg &&
mkdir -p /target/home/guest/Applications &&
mount -o noatime,compress=zstd:1,subvol=@guestapps /dev/mapper/vg-lv /target/home/guest/Applications &&



# Mount boot and efi
mount /dev/nvme0n1p2 /target/boot &&
mount /dev/nvme0n1p1 /target/boot/efi &&
 

# Getting helpers
wget gitlab.com/aviler/manteiga/-/raw/develop/insert-file.sh -O /tmp/insert-file.sh &&
wget gitlab.com/aviler/manteiga/-/raw/develop/fstab -O /tmp/fstab &&

chmod +x /tmp/insert-file.sh &&

./tmp/insert-file.sh /tmp/fstab /target/etc/fstab 10 &&

echo "check etc/fstab theres a extra line there @rootfs" &&

umount /mnt